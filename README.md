Builds orchestrator
===================

The role of this repository is to coordinate the nightly builds to:

1. ensure that Docker containers are built before starting the jobs that
   use them
2. generate a single build id to be shared by all builds
3. provide a central place to check the status of the nightly builds
